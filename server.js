const express = require('express');
const fs = require('fs');

const data = JSON.parse(fs.readFileSync('velociraptor.json', 'utf8'));

const app = express();

app.set('port', (process.env.PORT || 3001));

// Express only serves static assets in production
if (process.env.NODE_ENV === 'production') {
  app.use(express.static('client/build'));
}

app.get('/api/date/', (req, res) => {
  return res.json({data});
});

app.listen(app.get('port'), () => {
  console.log(`Find the server at: http://localhost:${app.get('port')}/`); // eslint-disable-line no-console
});

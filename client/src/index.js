import React from 'react';
import ReactDOM from 'react-dom';
import Banner from './Banner/Banner';

import './index.css';

ReactDOM.render(
  <Banner />,
  document.getElementById('root')
);

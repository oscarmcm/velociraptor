import React from 'react';
import './Clock.css';
import DateBetween from '../DateLib'


var Clock = React.createClass({

  getInitialState: function() {
    return { elapsedTime: '' };
  },

  componentDidMount: function() {
    this.tick();
    this.interval = setInterval(this.tick, 1000)
  },

  tick() {
    this.setState({
      elapsedTime: DateBetween(this.props.incidentDate, new Date())
    })
  },

  componentWillUnmount() {
    clearInterval(this.interval)
  },

  render: function() {
    return (
      <section className="Clock">
        <div className="Clock-days">
          <span className="Clock-digit">{this.state.elapsedTime.days}</span>
          <p className="Clock-label">Days</p>
        </div>
        <div className="Clock-hours">
          <span className="Clock-digit">{this.state.elapsedTime.hours}</span>
          <p className="Clock-label">Hours</p>
        </div>
        <div className="Clock-minutes">
          <span className="Clock-digit">{this.state.elapsedTime.minutes}</span>
          <p className="Clock-label">Minutes</p>
        </div>
        <div className="Clock-seconds">
          <span className="Clock-digit">{this.state.elapsedTime.seconds}</span>
          <p className="Clock-label">Seconds</p>
        </div>
      </section>
    );
  }
})

module.exports = Clock;
